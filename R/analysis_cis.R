#!/usr/bin/env Rscript

##################################################
# Analysis Cis
##################################################

library(optparse)
library(xseq)
library(readr)
library(dplyr)
library(xtable)

###
# Options
###

option_list = list(
  make_option(c("-c","--cancertype"), type='character', help = 'Cancer type code', default = "BRCA"),
  make_option(c("-p","--permute"), type='character', default = "FALSE"),
  make_option(c("-o","--outfile"), type='character', default = "/model.cis.RData"),
  make_option(c("-s","--seed"), type="numeric")
)
opt = parse_args(OptionParser(option_list = option_list))

cancer_type <- opt$cancertype
outfile_name <- opt$outfile
permute_samples <- opt$permute == "TRUE"
if(!is.null(opt$seed))
  set.seed(opt$seed)
  
###
# Load Expression Fit
###

load(file = paste0("data/expression/", cancer_type, "/expr.dis.RData"))
gene_subset <- colnames(expr.quantile)

###
# Read and filter mutations
###

# Filtering non expressed genes from mutations
mut_ <- rbind(read_tsv(paste0("data/SNV/", cancer_type, "/xseq_input_annotated.tsv")),
              read_tsv(paste0("data/Indels/", cancer_type, "/xseq_input_annotated.tsv")))
mut_ <- mut_ %>% filter(hgnc_symbol %in% gene_subset)
mut_ <- as.data.frame(mut_)
id = weight[mut_$hgnc_symbol] >= 0.8 &
  (mut_[, "variant_type"] %in% c("frameshift", "nonsense", "splice"))
id = id & !is.na(id)
mut.filt <- mut_[id,] %>% as.data.frame()

###
# Permute Samples
###

if(permute_samples){
  sample_names <- rownames(expr.quantile)
  permuted_sample_names <- setNames(sample(sample_names), sample_names)
  
  rownames(expr.quantile) <- permuted_sample_names[rownames(expr.quantile)]
  rownames(expr.dis.quantile$sample.prob.neutral) <- permuted_sample_names[rownames(expr.dis.quantile$sample.prob.neutral)]
  rownames(expr.dis.quantile$sample.prob.up.regulate) <- permuted_sample_names[rownames(expr.dis.quantile$sample.prob.up.regulate)]
  rownames(expr.dis.quantile$sample.prob.down.regulate) <- permuted_sample_names[rownames(expr.dis.quantile$sample.prob.down.regulate)]
}

##################################################
# Cis Analysis
##################################################

# Set prior
init = SetXseqPrior(expr.dis = expr.dis.quantile,
                    mut = mut.filt,
                    mut.type = "loss",
                    cis = T)

# Parameter constraints in EM
constraint = list(equal.fg = FALSE)

model.cis = InitXseqModel(mut = mut.filt,
                          expr = expr.quantile,
                          expr.dis = expr.dis.quantile,
                          cpd = init$cpd,
                          cis = TRUE,
                          prior = init$prior)

model.cis.em = LearnXseqParameter(model = model.cis,
                                  constraint = constraint, 
                                  iter.max = 50,
                                  threshold = 1e-6)

dir.create( paste0("data/", cancer_type), showWarnings = F)
save(model.cis, model.cis.em, file = paste0("data/", cancer_type, "/", outfile_name))

if(interactive()){
  xseq.pred = ConvertXseqOutput(model.cis.em$posterior)
  xseq.pred %>% group_by(hgnc_symbol) %>% summarise(PD = mean(`P(D)`)) %>% arrange(desc(PD))
}