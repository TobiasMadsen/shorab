#!/usr/bin/env Rscript

##################################################
# Run xseq 
# Trans analysis
# Make pseudo gene for enhancers
##################################################

library(xseq)
library(readr)
library(igraph)
library(dplyr)
library(optparse)

###
# Options
###

option_list = list(
  make_option(c("-c","--cancertype"), type='character', help = 'Cancer type code', default = "BRCA"),
  make_option(c("-n","--network"), type='character', default = "data/Networks/intpath_net_enh.RData"),
  make_option(c("-p","--permute"), type='character', default = "FALSE"),
  make_option(c("-o","--outfile"), type='character', default = "/model.trans.enhancer.RData"),
  make_option(c("-D","--downsample"), type='character', default = "FALSE")
)
opt = parse_args(OptionParser(option_list = option_list))

cancer_type <- opt$cancertype
# network_file_loc <- "data/Networks/intpath_net_enh_shuffled_1.RData"
network_file_loc <- opt$network
outfile_name <- opt$outfile
permute_samples <- opt$permute == "TRUE"
downsample <- as.logical(opt$downsample)

###
# Load expression data
###

load(file = paste0("data/expression/", cancer_type, "/expr.dis.RData"))
weight_enh <- weight
names(weight_enh) <- paste0(names(weight_enh), "_ENH")

###
# Permute Samples
###

if(permute_samples){
  sample_names <- rownames(expr.quantile)
  permuted_sample_names <- setNames(sample(sample_names), sample_names)
  
  rownames(expr.quantile) <- permuted_sample_names[rownames(expr.quantile)]
  rownames(expr.dis.quantile$sample.prob.neutral) <- permuted_sample_names[rownames(expr.dis.quantile$sample.prob.neutral)]
  rownames(expr.dis.quantile$sample.prob.up.regulate) <- permuted_sample_names[rownames(expr.dis.quantile$sample.prob.up.regulate)]
  rownames(expr.dis.quantile$sample.prob.down.regulate) <- permuted_sample_names[rownames(expr.dis.quantile$sample.prob.down.regulate)]
}

###
# Load Mutations and filter
###

gene_subset <- colnames(expr.quantile)

# Filtering non expressed genes from mutations
mut_enhancer <- read_tsv(paste0("data/enhancers/", cancer_type, "/enhancer_snv.tsv")) %>%
  filter(hgnc_symbol %in% gene_subset) %>%
  mutate(hgnc_symbol = paste0(hgnc_symbol,"_ENH"), enhancer = T)
mut_enhancer_indel <- read_tsv(paste0("data/enhancers/", cancer_type, "/enhancer_indel.tsv")) %>%
  filter(hgnc_symbol %in% gene_subset) %>%
  mutate(hgnc_symbol = paste0(hgnc_symbol,"_ENH"), enhancer = T)
mut_snv <- read_tsv(paste0("data/SNV/", cancer_type, "/xseq_input_annotated.tsv")) %>% mutate(enhancer = F)
mut_indel <- read_tsv(paste0("data/Indels/", cancer_type, "/xseq_input_annotated.tsv")) %>% mutate(enhancer = F)

mut_ <- rbind(mut_enhancer, mut_enhancer_indel, 
              mut_snv, mut_indel)
mut_ <- mut_ %>% filter(hgnc_symbol %in% gene_subset | enhancer)
mut_ <- as.data.frame(mut_)

id = c(weight, weight_enh)[mut_$hgnc_symbol] >= 0.8
id = id & !is.na(id)
mut.filt <- mut_[id,] %>% as.data.frame()

###
# Downsample to 10 mutations
###

if(downsample){
  mut.filt <- mut.filt %>% group_by(hgnc_symbol) %>% slice(sample(n(),min(10,n()))) %>% ungroup() %>%
    as.data.frame()
}

###
# Read network
###

network.env <- new.env()
load(file = network_file_loc, envir = network.env)
network <- get(names(network.env), envir = network.env)

###
# Estimate Trans-model
###

init = SetXseqPrior(expr.dis = expr.dis.quantile,
                    net = network,
                    mut = mut.filt,
                    mut.type = "both",
                    cis = F)

constraint <- list(equal.fg=TRUE, baseline=init$baseline)

model.trans = InitXseqModel(mut = mut.filt,
                            expr = expr.quantile,
                            net = network,
                            expr.dis = expr.dis.quantile,
                            cpd = init$cpd,
                            cis = F,
                            prior = init$prior)

model.trans.em <- LearnXseqParameter(model = model.trans,
                                     constraint = constraint,
                                     iter.max = 50,
                                     threshold = 1e-6)

save(model.trans.em, model.trans, file = paste0("data/", cancer_type, "/", outfile_name))
library(beepr); beep()

if(interactive()){
  library(stringr)
  xseq.pred = ConvertXseqOutput(model.trans.em$posterior)
  xseq.pred %>% group_by(hgnc_symbol) %>% summarise(PD = mean(`P(D)`)) %>% arrange(desc(PD))
  pd <- xseq.pred %>% group_by(hgnc_symbol) %>% summarise(PD = mean(`P(D)`)) %>% arrange(desc(PD)) %>% 
    filter(str_count(hgnc_symbol, "_ENH") > 0) %>% .$PD
}
