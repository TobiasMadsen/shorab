##################################################
# Estimate Expression 
##################################################

library(xseq)

for(cancer_type in c("BRCA","PACA","OV","CLLE")){
  expr_df <- read_tsv(paste0("data/expression/", cancer_type, "/expr.tsv"))
  expr_ <- as.matrix(expr_df[,-1])
  gene_subset <- colnames(expr_) #gene_subset <- intersect(colnames(xseq::expr), colnames(expr_))
  rownames(expr_) <- expr_df$donor_id
  expr_ <- expr_[,gene_subset]
  
  # Get Expression Distribution
  weight = EstimateExpression(expr_)
  expr_ = ImputeKnn(expr_)
  
  # Quantile Normalization
  expr.quantile = QuantileNorm(expr_)
  expr.dis.quantile = GetExpressionDistribution(expr=expr.quantile)
  save(expr.dis.quantile, expr.quantile, weight, file = paste0("data/expression/", cancer_type, "/expr.dis.RData"))
}
library(beepr)
beepr::beep()
