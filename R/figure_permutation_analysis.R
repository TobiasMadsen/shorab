##################################################
# Plot P(D)'s against Rank
# Trans + permuted networks
##################################################

library(reshape2)
library(stringr)
library(dplyr)
library(ggplot2)
library(xseq)
library(readr)

xseq_pred_list <- list()

for(f in list.files("data/BRCA/", pattern = "enhancer.(net([[:digit:]]).RData|RData)", full.names = T)){
  #load("data/BRCA/model.trans.enhancer.net3.RData")
  load(f)
  xseq.pred = ConvertXseqOutput(model.trans.em$posterior)

  xseq_pred_list[[f]] <- xseq.pred %>% group_by(hgnc_symbol) %>% summarise(PD = mean(`P(D)`)) %>% 
    arrange(desc(PD)) %>% mutate(Type = f, rank = 1:n())
}

gene_df <- do.call(rbind, xseq_pred_list) %>%
  mutate(permuted = as.logical(str_count(Type, pattern = "net")))

ggplot(gene_df, aes(x = rank, y = PD, group = Type, colour = permuted, alpha = permuted)) + geom_line() + 
  xlim(c(0,500)) + theme_bw() + scale_alpha_discrete(range=c(1,0.5))

pam50 <- read_tsv("~/git/irksome-fibula/data/PAM50.tab") %>% unlist()
gene_df %>% filter(as.character(hgnc_symbol) %in% pam50) %>%
  ggplot(aes(y = rank, x = Type, colour = permuted)) + geom_point()

##################################################
# Plot P(D)'s against Rank
# Cis + permuted samples
##################################################

xseq_pred_list <- list()

for(f in list.files("data/BRCA/", pattern = "model.cis*", full.names = T)){
  load(f)
  xseq.pred = ConvertXseqOutput(model.cis.em$posterior)
  
  xseq_pred_list[[f]] <- xseq.pred %>% group_by(hgnc_symbol) %>% summarise(PD = mean(`P(D)`)) %>% 
    arrange(desc(PD)) %>% mutate(Type = f, rank = 1:n())
}

gene_df <- do.call(rbind, xseq_pred_list) %>%
  mutate(permuted = as.logical(str_count(Type, pattern = "perm")))

ggplot(gene_df, aes(x = rank, y = PD, group = Type, colour = permuted, alpha = permuted)) + geom_line() + 
  xlim(c(0,100)) + theme_bw() + scale_alpha_discrete(range=c(1,0.5))

##################################################
# Plot P(D)'s against Rank
# Trans + permuted samples
##################################################

xseq_pred_list <- list()

for(f in list.files("data/BRCA/", pattern = "enhancer.(perm([[:digit:]]).RData|RData)", full.names = T)){
  #load("data/BRCA/model.trans.enhancer.net3.RData")
  load(f)
  xseq.pred = ConvertXseqOutput(model.trans.em$posterior)
  
  xseq_pred_list[[f]] <- xseq.pred %>% group_by(hgnc_symbol) %>% summarise(PD = mean(`P(D)`)) %>% 
    arrange(desc(PD)) %>% mutate(Type = f, rank = 1:n())
}

gene_df <- do.call(rbind, xseq_pred_list) %>%
  mutate(permuted = as.logical(str_count(Type, pattern = "perm")))

ggplot(gene_df, aes(x = rank, y = PD, group = Type, colour = permuted, alpha = permuted)) + geom_line() + 
  xlim(c(0,500)) + theme_bw() + scale_alpha_discrete(range=c(1,0.5))

gene_df %>% filter(as.character(hgnc_symbol) %in% pam50) %>%
  ggplot(aes(y = rank, x = Type, colour = permuted)) + geom_point()


