##################################################
# Summary statistics on enhancers
##################################################

library(readr)
library(dplyr)

###
# Read hits. Intersected with enhancers
###

enhancerHits <- read_tsv("data/SNV/enhancerHits.tsv", col_names = F)
colnames(enhancerHits) <- c('chr', 'start', 'end', 'hgnc_symbol', 'aliquot_id', 'cancer_type') 

enhancerHits <- enhancerHits %>% 
  mutate(cancer_type_split = str_locate(cancer_type, '-')[,1] - 1) %>%
  mutate(cancer_type = substr(cancer_type, 1, cancer_type_split)) %>%
  dplyr::select(-cancer_type_split)

# Table of hits pr. cancer type
enhancerHits %>% 
  group_by(cancer_type) %>% summarize(n = n()) %>% arrange(desc(n)) %>%
  print(n = 38)

###
# Filter on cancer type and assign donor id
###

for(cancer_type in c("BRCA","PACA","OV","CLLE")){
  sample_table <- read_tsv("data/sample_information/sample_table.tsv") %>%
    filter(! is.na(rna_seq_star_id)) %>% filter(cancer_type == get("cancer_type"))
  
  aliquotToDonor <- sample_table$icgc_donor_id
  names(aliquotToDonor) <- sample_table$wgs_id
  
  enhancerHitsCancerType <- enhancerHits %>% filter(cancer_type == get("cancer_type")) %>%
    mutate(donor = aliquotToDonor[aliquot_id]) %>% filter(!is.na(donor)) %>%
    group_by(donor, hgnc_symbol) %>% summarise() %>% ungroup %>%
    transmute(sample = donor, hgnc_symbol = hgnc_symbol, variant_type = "nonsense")
  
  dir.create(paste0("data/enhancers/", cancer_type))
  write.table(enhancerHitsCancerType, file = paste0("data/enhancers/", cancer_type, "/enhancer_snv.tsv"), quote = F,
              sep = "\t", row.names = F)
  
  ###
  # Read indels 
  ###
  
  enhancerHitsIndel <- read_tsv(paste0("data/Indels/", cancer_type, "/enhancerHits.tsv"), col_names = F)
  colnames(enhancerHitsIndel) <- c('chr', 'start', 'end', 'hgnc_symbol', 'donor') 
  enhancerHitsIndelCancerType <- enhancerHitsIndel %>% transmute(sample = donor, hgnc_symbol, variant_type = "nonsense")
  write.table(enhancerHitsIndelCancerType, file = paste0("data/enhancers/", cancer_type, "/enhancer_indel.tsv"), quote = F,
              sep = "\t", row.names = F)
}
