##################################################
# Test xseq
##################################################

library(xseq)

data(mut, expr, cna.call, cna.logr, net)

# Get Expression Distribution
weight = EstimateExpression(expr)

# Impute missing values
expr = ImputeKnn(expr)
cna.logr = ImputeKnn(cna.logr)

# Quantile-Normalization (May already be performed)
expr.quantile = QuantileNorm(expr)

tmp = GetExpressionDistribution(expr=expr.quantile, mut=mut, cna.call=cna.call,
                                gene="TP53", show.plot=TRUE)
expr.dis.quantile = GetExpressionDistribution(expr=expr.quantile, mut=mut, cna.call=cna.call)

# Filtering mutations in non expressed genes and only loss-of-function
id = weight[mut[, "hgnc_symbol"]] >= 0.8 & (mut[, "variant_type"] %in% c("FRAMESHIFT", "NONSENSE", "SPLICE")) 
id = id & !is.na(id)
mut.filt = mut[id, ]

# Set prior
init = SetXseqPrior(expr.dis = expr.dis.quantile,
                    mut = mut.filt,
                    mut.type = "loss",
                    cis = T)

# Parameter constraints in EM
constraint = list(equal.fg = FALSE)

model.cis = InitXseqModel(mut = mut.filt,
                          expr = expr.quantile,
                          expr.dis = expr.dis.quantile,
                          cpd = init$cpd,
                          cis = TRUE,
                          prior = init$prior)

model.cis.em = LearnXseqParameter(model = model.cis,
                                  constraint = constraint, 
                                  iter.max = 50,
                                  threshold = 1e-6)

xseq.pred = ConvertXseqOutput(model.cis.em$posterior)
