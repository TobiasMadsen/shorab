#!/usr/bin/env bash

# Convert enhancers to BED format

awk -F '\t' 'BEGIN{OFS = "\t"}; NR > 1 {if($2 != "" && $2 != "NA") print $3,$4,$5,$2}' data/enhancers/enh.tier1.tier2.gene.centric.txt > data/enhancers/enh.tier1.tier2.bed

# Pipe mutations in BED format to Bedtools intersect
awk 'OFS = "\t" {print $1, $2, $2+1, $5, $6}' data/SNV/sorted_mar2016.tsv | bedtools intersect -a data/enhancers/enh.tier1.tier2.bed -b stdin -wb | awk 'OFS = "\t" {print $1,$2,$3,$4,$8,$9}' > data/SNV/enhancerHits.tsv

# Pipe indel mutations in BED format to Bedtools intersect
awk 'OFS = "\t" {print "chr"$1, $2, $3, $6}' data/Indels/BRCA/mut.avinput | bedtools intersect -a data/enhancers/enh.tier1.tier2.bed -b stdin -wb | awk 'OFS="\t" {print $1, $2,$3,$4,$8}' > data/Indels/BRCA/enhancerHits.tsv

awk 'OFS = "\t" {print "chr"$1, $2, $3, $6}' data/Indels/OV/mut.avinput | bedtools intersect -a data/enhancers/enh.tier1.tier2.bed -b stdin -wb | awk 'OFS="\t" {print $1, $2,$3,$4,$8}' > data/Indels/OV/enhancerHits.tsv

awk 'OFS = "\t" {print "chr"$1, $2, $3, $6}' data/Indels/PACA/mut.avinput | bedtools intersect -a data/enhancers/enh.tier1.tier2.bed -b stdin -wb | awk 'OFS="\t" {print $1, $2,$3,$4,$8}' > data/Indels/PACA/enhancerHits.tsv

awk 'OFS = "\t" {print "chr"$1, $2, $3, $6}' data/Indels/CLLE/mut.avinput | bedtools intersect -a data/enhancers/enh.tier1.tier2.bed -b stdin -wb | awk 'OFS="\t" {print $1, $2,$3,$4,$8}' > data/Indels/CLLE/enhancerHits.tsv





