from gwf import *

# Cis analysis
cis_analysis = template(
    output = "data/{cancer_type}/{outfile_name}",
    input = ["data/expression/{cancer_type}/expr.dis.RData",
             "data/SNV/{cancer_type}/xseq_input_annotated.tsv",
             "data/Indels/{cancer_type}/xseq_input_annotated.tsv"],
    walltime = "00:10:00") << '''
./R/analysis_cis.R -c {cancer_type} -p {permute} -o {outfile_name}
'''

# Trans analysis with enhancers
trans_analysis = template(
    output = "data/{cancer_type}/{outfile_name}",
    input = ["data/expression/{cancer_type}/expr.dis.RData",
             "data/SNV/{cancer_type}/xseq_input_annotated.tsv",
             "data/Indels/{cancer_type}/xseq_input_annotated.tsv",
             "data/enhancers/{cancer_type}/enhancer_snv.tsv",
             "data/enhancers/{cancer_type}/enhancer_indel.tsv",
             "{network}"],
    walltime = "02:00:00") << '''
./R/analysis_trans_enhancer.R -c {cancer_type} -p {permute} --network "{network}" -o {outfile_name}
'''

# Trans analysis Downsampled
trans_analysis_downsample = template(
    output = "data/{cancer_type}/{outfile_name}",
    input = ["data/expression/{cancer_type}/expr.dis.RData",
             "data/SNV/{cancer_type}/xseq_input_annotated.tsv",
             "data/Indels/{cancer_type}/xseq_input_annotated.tsv",
             "data/enhancers/{cancer_type}/enhancer_snv.tsv",
             "data/enhancers/{cancer_type}/enhancer_indel.tsv",
             "{network}"],
    walltime = "02:00:00") << '''
./R/analysis_trans_enhancer.R -c {cancer_type} -D TRUE --network "{network}" -o {outfile_name}
'''

