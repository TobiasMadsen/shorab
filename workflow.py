# Specify workflows from toplevel directory using GWF
# See http://mailund.github.io/gwf//

from gwf import *
import os.path
from os import getcwd
sys.path.append(getcwd()) # Somehow not in path when running gwf on cluster
from templates import *

##################################################
# Obtain Data
##################################################

target("getReactomeDB",
       output = "data/Networks/Reactome/homo_sapiens.interactions.txt") << '''
mkdir -p data/Networks/Reactome/
wget -P data/Networks/Reactome/ http://www.reactome.org/download/current/homo_sapiens.interactions.txt.gz
gunzip data/Networks/Reactome/homo_sapiens.interactions.txt.gz
'''

##################################################
# Run cis analysis
##################################################

# Cis BRCA
target("cis_BRCA") << cis_analysis(cancer_type = "BRCA", permute = "FALSE", outfile_name = "model.cis.RData")
target("cis_BRCA_perm0") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_0.RData")
target("cis_BRCA_perm1") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_1.RData")
target("cis_BRCA_perm2") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_2.RData")
target("cis_BRCA_perm3") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_3.RData")
target("cis_BRCA_perm4") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_4.RData")
target("cis_BRCA_perm5") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_5.RData")
target("cis_BRCA_perm6") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_6.RData")
target("cis_BRCA_perm7") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_7.RData")
target("cis_BRCA_perm8") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_8.RData")
target("cis_BRCA_perm9") << cis_analysis(cancer_type = "BRCA", permute = "TRUE", outfile_name = "model.cis.permute_9.RData")

# Trans BRCA
target("trans_BRCA") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.RData", 
                                       permute = "FALSE",
                                       network = "data/Networks/intpath_net_enh.RData")

# Trans BRCA permuted networks
target("trans_BRCA_net0") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net0.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_0.RData")
target("trans_BRCA_net1") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net1.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_1.RData")
target("trans_BRCA_net2") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net2.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_2.RData")
target("trans_BRCA_net3") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net3.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_3.RData")
target("trans_BRCA_net4") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net4.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_4.RData")
target("trans_BRCA_net5") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net5.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_5.RData")
target("trans_BRCA_net6") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net6.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_6.RData")
target("trans_BRCA_net7") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net7.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_7.RData")
target("trans_BRCA_net8") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net8.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_8.RData")
target("trans_BRCA_net9") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.net9.RData", 
                                            permute = "FALSE",
                                            network = "data/Networks/intpath_net_enh_shuffled_9.RData")

# Trans BRCA permuted samples
target("trans_BRCA_perm0") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm0.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm1") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm1.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm2") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm2.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm3") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm3.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm4") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm4.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm5") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm5.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm6") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm6.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm7") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm7.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm8") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm8.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_perm9") << trans_analysis(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.perm9.RData", 
                                       permute = "TRUE",
                                       network = "data/Networks/intpath_net_enh.RData")

# Trans BRCA Downsample
target("trans_BRCA_down0") << trans_analysis_downsample(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.down0.RData", 
                                                        network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_down1") << trans_analysis_downsample(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.down1.RData", 
                                                        network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_down2") << trans_analysis_downsample(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.down2.RData", 
                                                        network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_down3") << trans_analysis_downsample(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.down3.RData", 
                                                        network = "data/Networks/intpath_net_enh.RData")
target("trans_BRCA_down4") << trans_analysis_downsample(cancer_type = "BRCA", outfile_name = "model.trans.enhancer.down4.RData", 
                                                        network = "data/Networks/intpath_net_enh.RData")
